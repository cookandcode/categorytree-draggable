require('./bootstrap');
import 'bootstrap';
import Form from './utilities/Form';

import { createApp } from 'vue';

import CategoryTreeDraggable from "./components/CategoryTreeDraggable";


const app = createApp( {
        components: {

            CategoryTreeDraggable
        }
    }
)

app.mount('#app')

window.Form = Form;
