<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'slug',
        'description',
        'parent_id',
        'sort_order',
        'activated',
    ];

    //TODO: $dates/$casts-Attributen?

    /**
     * Get the subcategories for a category
     */
    public function subcategory()
    {
        return $this->hasMany(Category::class, 'parent_id');
    }

}
